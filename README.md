# Film Service Web Application

## Technologies
1. [Spring & Spring Boot](https://spring.io/)
2. [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
3. [Hibernate](https://hibernate.org/)
4. [CRUD Repository](https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html)
5. [MySQL](https://www.mysql.com/)
6. [Google App Engine](https://cloud.google.com/appengine/)
7. [Google Cloud SQL](https://cloud.google.com/sql/docs/)
8. [Maven](https://maven.apache.org/index.html)

## Run Locally
#### Database Set Up
If you are running the app for the first time then you will need to firstly set up your database.
This can be done in one of two ways. 
1. First Option. Create a local database using something like MySQL Workbench. Use the schema and data provided 
in resources > db > mysql to build the database and add a few films. Change the URL, Username and Password 
under the Local Database Credentials heading in application-mysql.properties to that of your newly created database.
2. Second Option. Uncomment the spring.datasource.initialization-mode=always line in application-mysql.properties
Change the URL, Username and Password under the Local Database Credentials heading to match that of an existing local
database. Spring will create the relevant tables and add some data for you when you first run the app. Remember to 
re comment out spring.datasource.initialization-mode=always the second time you start up the app.

#### Build the Application
This app uses Maven as its build automation tool. Use Maven to build the application locally. Run the command
`mvn package`
For more information on Maven visit the Maven website [Here](https://maven.apache.org/index.html)

#### Run Spring Boot Application
To run the app as a Spring Boot Application run the command `mvn spring-boot:run`
Once the process has finished you can use the application by visiting [http://localhost:8080]

## Run In The Cloud
Use the GCloud documents [Here](https://cloud.google.com/resource-manager/docs/creating-managing-projects) to set up a new
project. 

#### Database Set Up
To set up your database to run this application on the Google Cloud Platform, refer to the GCloud documents
[Here](https://cloud.google.com/sql/docs/mysql/create-manage-databases) As above, you can use the provided schema and 
data in resources > db > mysql to build the database and add a few films. Once this is done, change the database credentials
under the header Cloud Database Credentials in the application-mysql.properties file to match that of your create sql database.

#### Deploy to the cloud
This project uses the cloud SDK-based AppEngine plugin for Maven to deploy the application to the cloud. To use this you 
will firstly need to download the SDK from [Here](https://cloud.google.com/sdk/install) and initialize it to use your project.
Once that is done, simply run the command `mvn appengine:deploy` from within the project to deploy to the GCP.

Use the link provided in the terminal to view the app in the cloud.

To change the cloud config e.g. number of instance, memory and scaling, update the app.yaml file in src > main > appengine.

