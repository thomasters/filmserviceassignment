CREATE DATABASE IF NOT EXISTS `filmservice_db`;
USE `filmservice_db`;

CREATE TABLE IF NOT EXISTS `films` (
`id` bigint(5) NOT NULL AUTO_INCREMENT,
`film_name` varchar(200) NOT NULL,
`film_year` varchar(100) NOT NULL,
`film_genre` varchar(100) NOT NULL,
`film_director` varchar(100) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB;