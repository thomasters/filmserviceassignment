INSERT INTO films (id, film_name, film_year, film_genre, film_director)
VALUES  (1, 'Into The Wild', '1997', 'Drama/Adventure', 'Sean Penn'),
        (2, 'Titanic', '2007', 'Drama/Disaster', 'James Cameron'),
        (3, 'Rise of the Footsoldier', '2008', 'Drama/Docudrama', 'Julian Gilbey');