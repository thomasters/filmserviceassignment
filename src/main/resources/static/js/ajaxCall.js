/**
 * This class contains all of the Ajax calls to the backend.
 * There is a function for each type of call; post, get. update and delete
 */


/**
 * POST request
 */
function fire_ajax_post() {
    $("#btn-add").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "film",
        data: getFilmData(),
        cache: false
    })
        .done(function (){
            var feedback = "<div class=\"alert alert-success\" role=\"alert\">Film successfully added!</div>";
            $('#add-film').html(feedback);
        })
        .fail(function (){
            var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Something went wrong!</div>";
            $('#add-film').html(feedback);
        })
        .always(function (){$('#btn-add').prop("disabled", false)});
}

/**
 * GET request
 */
function fire_ajax_get(url, dataType, outputAddress, buttonID) {
    $(buttonID).prop("disabled", true);
    var dtype = dataType;
    if(dataType === "text"){dataType = "json"}
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: url,
        dataType: dataType,
        cache: false,
        error: function() {
            // This is necessary to catch errors and activate the fail block.
        }
    })
        .done(function (data, textStatus, jqXHR){
            var divId = getDivId(url);
            $(outputAddress).html("<div id='"+divId+"' class='alert alert-success' role='alert'></div>");
            if(dtype === "xml"){
                outputToUser(divId, jqXHR.responseText);
            } else if (dtype === "json"){
                outputToUser(divId, JSON.stringify(data, null, 4));
            } else if (dtype === "text") {
                outputToUser(divId, filmToText(JSON.stringify(data, null, 4)));
            }
        })
        .fail(function (){
            var divId = getDivId(url);
            $(outputAddress).html("<div id='"+divId+"' class='alert alert-warning' role='alert'>Something went wrong!</div>");
        })
        .always(function (){$(buttonID).prop("disabled", false)});
}

/**
 * DELETE request
 */
function fire_ajax_delete(id){
    $("#btn-delete").prop("disabled", true);

    $.ajax({
        type: "DELETE",
        url: "film/" + id,
        cache: false
    })
        .done(function (){
            var feedback = "<div class=\"alert alert-success\" role=\"alert\">Film successfully deleted!</div>"
            $('#delete-film').html(feedback);
        })
        .fail(function (){
            var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Something went wrong!</div>"
            $('#delete-film').html(feedback);
        })
        .always(function (){$('#btn-delete').prop("disabled", false)});
}

/**
 * UPDATE request
 */
function fire_ajax_update() {
    $("#btn-update").prop("disabled", true);

    $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "film",
        data: getFilmUpdateData(),
        cache: false
    })
        .done(function (){
            var feedback = "<div class=\"alert alert-success\" role=\"alert\">Film successfully updated!</div>"
            $('#update-film').html(feedback);
        })
        .fail(function (){
            var feedback = "<div class=\"alert alert-warning\" role=\"alert\">Something went wrong!</div>"
            $('#update-film').html(feedback);
        })
        .always(function (){$('#btn-update').prop("disabled", false)});
}