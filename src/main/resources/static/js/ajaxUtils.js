/**
 * This is a Utils class needed to prepare the relevant data to been sent to the restful serivce, and parse the dat
 * returned by the service.
 */

/**
 * This funtion build the ajax get request prior to calling the fireAjaGet function.
 * @param selectID
 * @param getFilmId
 * @param getSearch
 */
function build_ajax_get(selectID, getFilmId, getSearch) {
    var dataType = getSelectedOption(selectID).toLowerCase();
    var id = getValue(getFilmId);
    var search = getValue(getSearch);

    if(id) {
        fire_ajax_get("film/" + id, dataType, "#list-film", "#btn-get-film-id");
    }else if(search){
        fire_ajax_get("films?search=" + search, dataType, "#list-film-search", "#btn-get-film-search");
    }else{
        fire_ajax_get("films", dataType, '#list-films', "#btn-list");
    }
}

/**
 * Outputs the given text to the given div ID.
 * @param divId
 * @param responseText
 */
function outputToUser(divId, responseText) {
    divId = "#"+divId;
    $(divId).text(responseText);
}

/**
 * This function turns the given JSON object into plain text to output to the user.
 * @param jData
 * @returns {string|*}
 */
function filmToText(jData) {
    var separator = ', ';
    return ConvertToCSV(jData, separator);
}

/**
 * Turns the given json data into a list of comma separated values.
 * @param jData
 * @param delimiter
 * @returns {string}
 * @constructor
 */
function ConvertToCSV(jData, delimiter) {
    jData = toArray(jData);
    var arrJSON = typeof jData != 'object' ? JSON.parse(jData) : jData;

    var sReturnVal = '';
    for (var i = 0; i < arrJSON.length; i++) {
        var sLine = '';
        for (var index in arrJSON[i]) {
            if (sLine !== '') sLine += delimiter;
            sLine += arrJSON[i][index];
        }
        sReturnVal += sLine + '\r\n';
    }
    return sReturnVal;
}

/**
 * This function ensures that the json data provided is a json array and not just (in this case) a single film.
 * The data is needed in the form of an array for the ConvertToCSV method to be able to work.
 * @param jData
 * @returns {string|*}
 */
function toArray(jData){
    if(jData.trim().charAt(0)==="["){
        return jData;
    } else if (jData.trim().charAt(0)==="{"){
        return "["+jData+"]";
    }
}

/**
 * Gets all of the data from the add film fields and turns in into a JSON String.
 * @returns {string}
 */
function getFilmData() {
    var search = {};
    search["name"] = $("#name").val();
    search["year"] = $("#year").val();
    search["genre"] = $("#genre").val();
    search["director"] = $("#director").val();
    return JSON.stringify(search);
}

/**
 * Get all of the data in the film update fields anf turns it into a JSON String.
 * @returns {string}
 */
function getFilmUpdateData() {
    var update = {};
    update["id"] = $("#update-id").val();
    update["name"] = $("#update-name").val();
    update["year"] = $("#update-year").val();
    update["genre"] = $("#update-genre").val();
    update["director"] = $("#update-director").val();
    return JSON.stringify(update);
}

/**
 * Gets the value of the select option linked to the given ID.
 * @param selectID - ID of the HTML select option
 * @returns {string|null} The value of the option.
 */
function getSelectedOption(selectID) {
    var element = document.getElementById(selectID);
    return element.options[element.selectedIndex].text;
}

/**
 * Gets the value of the user input in the input field linked the the provided ID.
 * @param id - ID of the HTML input.
 * @returns {string|null} The value of the input.
 */
function getValue(id) {
    if(id){
        return(encodeURI(document.getElementById(id).value));
    } else {
        return null;
    }
}

/**
 * Uses the URL to decide what div the output should be displayed in.
 * @param url - URL that will be given to the Ajax call.
 * @returns {string} Div ID for the output to be displayed in.
 */
function getDivId(url){
    if(url.includes("search")){
        return "insert-search";
    }else if(url.includes("films")){
        return "insert-films";
    } else if (url.includes("film")){
        return "insert-film";
    }
}

/**
 * Checks with the user via an on screen alert that they are sure they want to delete the film from
 * the database.
 * @param id - Film of the ID being deleted.
 */
function confirmDelete(id) {
    var result = confirm("Are you sure that you want to delete?");
    if (result) {
        fire_ajax_delete(getValue(id));
    }
}