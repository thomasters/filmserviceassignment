package com.thomasters.filminfoservice.utils;

import com.thomasters.filminfoservice.entities.Film;
import com.thomasters.filminfoservice.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class holds the business logic for the controller to check that data provided by the client is valid.
 */
@Service
public class FilmValidator {

    private final FilmRepository filmRepository;

    @Autowired
    FilmValidator(FilmRepository filmRepository){
        this.filmRepository = filmRepository;
    }

    /**
     * Checks that the provided film has all of the necessary data.
     * @param film - Film entity.
     * @return - TRUE if all of the fields in the film are populated,
     *          FALSE if any of the fields in the provided film are empty or null.
     */
    public boolean filmDataIsComplete(Film film){
        return isNotNullOrEmpty(film.getName()) &&
                isNotNullOrEmpty(film.getYear()) &&
                isNotNullOrEmpty(film.getDirector()) &&
                isNotNullOrEmpty(film.getGenre());
    }

    /**
     * Checks that the provided film has any missing data.
     * @param film - Film entity.
     * @return - TRUE if any of the fields in the provided film are empty or null.
     *          FALSE if all of the fields in the film are populated,
     */
    public boolean filmDataIsIncomplete(Film film){
        return (!filmDataIsComplete(film));
    }

    /**
     * Checks to see if a film exists in the database with the provided ID.
     * @param id - ID of the film to check.
     * @return TRUE if a film with the provided ID exists in the database.
     *         FALSE if a film with the provided ID does not exist in the database.
     */
    public boolean filmWithIdExists(int id){
        return filmRepository.existsById(id);
    }

    /**
     * Checks to see if a film does not exists in the database with the provided ID.
     * @param id - ID of the film to check.
     * @return TRUE if a film with the provided ID does not exist in the database.
     *         FALSE if a film with the provided ID exists in the database.
     */
    public boolean filmWithIdDoesNotExist(int id){
        return !filmWithIdExists(id);
    }

    /**
     * Checks the database for films that have identical attributes to the one provided.
     * Essentially checking for duplicate films.
     * DOES NOT CHECK FOR MATCHING ID.
     * @param film - Film to check against
     * @return TRUE if a duplicate film exists in the database.
     *         False if a duplicate film does not exist in the database.
     */
    public boolean filmWithDetailsExists(Film film){
        return filmRepository.findByNameAndDirectorAndYearAndGenre(film.getName(), film.getDirector(), film.getYear(), film.getGenre()).size()>0;
    }

    /**
     * Checks the provided String to see if it contains a valid value.
     * @param str - String to be checked.
     * @return TRUE if String does not contain valid value.
     *         FALSE if String does contain valid value.
     */
    public boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * Checks the provided String to see if it does not contains a valid value.
     * @param str - String to be checked.
     * @return TRUE if String does contain valid value.
     *         FALSE if String does not contain valid value.
     */
    public boolean isNotNullOrEmpty(String str) {
        return !isNullOrEmpty(str);
    }

}
