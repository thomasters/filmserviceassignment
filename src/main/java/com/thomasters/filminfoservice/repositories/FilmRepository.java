package com.thomasters.filminfoservice.repositories;

import com.thomasters.filminfoservice.entities.Film;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
 */
@Repository
public interface FilmRepository extends CrudRepository<Film, Integer> {

    /**
     * Custom query. Finds all films in the database that match all of the parameters provided.
     * @param filmName - Name of the film
     * @param filmDirector - Director of the film
     * @param filmYear - Year the film was released
     * @param genre - Genre of the film
     * @return List of matching films.
     */
    List<Film> findByNameAndDirectorAndYearAndGenre(String filmName, String filmDirector, String filmYear, String genre);

    /**
     * Custom query. Finds all films in the database that contain a provided key word.
     * @param filmName - Name of the film
     * @param filmDirector - Director of the film
     * @param year - Year the film was released
     * @param genre - Genre of the film
     * @return - List of matching films.
     */
    List<Film> findFilmsByNameContainingOrDirectorContainingOrYearContainingOrGenreContaining(String filmName, String filmDirector, String year, String genre);

}