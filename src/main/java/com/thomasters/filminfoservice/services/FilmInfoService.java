package com.thomasters.filminfoservice.services;

import com.thomasters.filminfoservice.entities.Film;
import com.thomasters.filminfoservice.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implements IFilmInfoService. This class contains all of the business logic between the
 * controller and the repository.
 */
@Service
public class FilmInfoService implements IFilmInfoService {

    private final FilmRepository filmRepository;

    @Autowired
    public FilmInfoService(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    /**
     * Gets all of the films in the database and returns them in a list.
     * @return - List of all of the films int he database.
     */
    @Override
    public List<Film> getAllFilms() {
        List<Film> list = new ArrayList<>();
        filmRepository.findAll().forEach(list::add);
        return list;
    }

    /**
     * Gets all films in the database that contain the provided String in it name, director, year or genre.
     * @param search - key word to search for.
     * @return - List of films that contain that keyword.
     */
    @Override
    public List<Film> getFilmsBySearch(String search) {
        return new ArrayList<>(filmRepository.findFilmsByNameContainingOrDirectorContainingOrYearContainingOrGenreContaining
                (search, search, search, search));
    }

    /**
     * Gets the film from the database that matches the given ID.
     * @param filmId - The ID of the requested film
     * @return - Film with matching ID.
     */
    @Override
    public Film getFilmById(int filmId) {
        Optional<Film> film = filmRepository.findById(filmId);
        return film.orElse(null);
    }

    /**
     * Adds the given film to the database.
     * @param film - Film to be added to the database.
     */
    @Override
    public void addFilm(Film film) {
        filmRepository.save(film);
    }

    /**
     * Updates the film in the database with the matching ID with the rest of the data in the provided Film entity.
     * @param film - Film to be updated in the database.
     */
    @Override
    public void updateFilm(Film film) {
        filmRepository.save(film);
    }

    /**
     * Deletes the film from the database that has an ID matching the one provided.
     * @param filmId - ID of the film to be deleted from the database.
     */
    @Override
    public void deleteFilm(int filmId) {
        filmRepository.deleteById(filmId);
    }

}
