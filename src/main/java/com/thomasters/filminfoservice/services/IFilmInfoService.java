package com.thomasters.filminfoservice.services;

import com.thomasters.filminfoservice.entities.Film;

import java.util.List;

/**
 * This interface contains all of the necessary method declarations needed by the service.
 */
public interface IFilmInfoService {
    List<Film> getAllFilms();
    List<Film> getFilmsBySearch(String search);
    Film getFilmById(int filmId);
    void addFilm(Film film);
    void updateFilm(Film film);
    void deleteFilm(int filmId);
}
