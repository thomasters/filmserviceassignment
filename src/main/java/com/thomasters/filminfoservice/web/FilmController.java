package com.thomasters.filminfoservice.web;

import com.thomasters.filminfoservice.entities.Film;
import com.thomasters.filminfoservice.services.IFilmInfoService;
import com.thomasters.filminfoservice.utils.FilmValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is the rest controller for the web service.
 */
@RestController
public class FilmController {

    private final IFilmInfoService filmInfoService;

    private final FilmValidator validator;

    /**
     * Use of constructor injection over field injection as per recommended technique.
     * @param filmInfoService
     * @param validator
     */
    @Autowired
    public FilmController(IFilmInfoService filmInfoService, FilmValidator validator) {
        this.filmInfoService = filmInfoService;
        this.validator = validator;
    }

    /**
     * GET request - url/film/{id}
     * This get request will check to see if the provided ID matches a film in the database.
     * If a film exists then the film will be returned with a 200 okay status. If the film does not
     * exist then it will return a 404 not found status.
     * @param id - ID of the requested film
     * @return - Response Entity. The film (if found) and the HTTP status of the request.
     */
    @GetMapping("film/{id}")
    public ResponseEntity<Object> getFilmByID(@PathVariable("id") Integer id,
                                            @RequestHeader(name = "Accept", required = false) String acceptHeader){
        if(validator.filmWithIdExists(id)){
            if(acceptHeader.equals("text/plain")){
                return new ResponseEntity<>(filmInfoService.getFilmById(id).toString(), HttpStatus.OK);
            }
            return new ResponseEntity<>(filmInfoService.getFilmById(id), HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * 1. GET request - url/films or
     * 2. GET request - url/films?search={search}
     *
     * 1. This get request will return a list of all of the films in the database. An Empty list will be returned if the
     * database does not contain any films.
     * 2. This request will check to see if the provided search string is null or empty. If it is it will return a HTTP
     * status 400 bad request.
     * If the provided search string is valid then it will search the database for a film that contains that string in
     * it name, director, year or genre.
     * @param search (Not required) - key word to be searched for.
     * @return - Response Entity - List of films and HTTP status of the request.
     */
    @GetMapping("films")
    public ResponseEntity<Object> getAllFilms(@RequestParam(required = false) String search,
                                                  @RequestHeader(name = "Accept", required = false) String acceptHeader) {
        if(search != null) {
            if(validator.isNullOrEmpty(search)){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else {
                return createObjectResponseEntity(acceptHeader, filmInfoService.getFilmsBySearch(search));
            }
        } else {
            return createObjectResponseEntity(acceptHeader, filmInfoService.getAllFilms());
        }
    }

    private ResponseEntity<Object> createObjectResponseEntity
            (@RequestHeader(name = "Accept", required = false) String acceptHeader, List<Film> listOfFilms) {
        if(listOfFilms.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            if(acceptHeader.equals("text/plain")){
                return new ResponseEntity<>(getListAsText(listOfFilms), HttpStatus.OK);
            }
            return new ResponseEntity<>(listOfFilms, HttpStatus.OK);
        }
    }

    private Object getListAsText(List<Film> listOfFilms) {
        return listOfFilms.stream().map(Object::toString).collect(Collectors.joining(",\n"));
    }

    /**
     * POST request - url/film
     * This post request checks that the film provided has all of the necessary data. If not it returns a HTTP status
     * of 400 bad request.
     * It also checks that the provided film is not a duplicate. If it is a duplicate then it returns a HTTP status of
     * 409 conflict.
     * If the film has been added successfully then it will return a HTTP status of 201 created.
     * @param film - The film to be added to the database.
     * @return Response Entity - HTTP status of the request.
     */
    @PostMapping("film")
    public ResponseEntity<Void> addFilm(@RequestBody Film film) {
        if (validator.filmDataIsIncomplete(film)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (validator.filmWithDetailsExists(film)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        filmInfoService.addFilm(film);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * PUT request - url/film
     * This request checks that the film provided has all of the necessary data. If not it returns a HTTP status
     * of 400 bad request.
     * It check that a film with a matching ID already exists in the database and if not it returns a HTTP status
     * of 404 not found.
     * It also checks that the provided film is not a duplicate. If it is a duplicate then it returns a HTTP status of
     * 409 conflict.
     * If the film has been updated successfully then a HTTP status of 200 okay will be returned.
     * @param film - The film to be updated
     * @return - Response Entity - The update film details (if successful) and HTTP status of the request.
     */
    @PutMapping("film")
    public ResponseEntity<Film> updateFilm(@RequestBody Film film) {
        if (validator.filmDataIsIncomplete(film)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(validator.filmWithIdDoesNotExist(film.getId())){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(validator.filmWithDetailsExists(film)){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        filmInfoService.updateFilm(film);
        return new ResponseEntity<>(film, HttpStatus.OK);
    }

    /**
     * DELETE request - url/film/{id}
     * This request checks that a film with the provided ID exists in the database. If the film does not exist then a
     * HTTP status of 404 not found is returned.
     * If the film was successfully deleted then it will return a HTTP status 200 okay.
     * @param id - ID of the film to be deleted.
     * @return - Response Entity - HTTP status of the request.
     */
    @DeleteMapping("film/{id}")
    public ResponseEntity<Void> deleteFilm(@PathVariable("id") Integer id) {
        if(validator.filmWithIdDoesNotExist(id)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        filmInfoService.deleteFilm(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
