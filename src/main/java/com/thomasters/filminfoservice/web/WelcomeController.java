package com.thomasters.filminfoservice.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is the controller for which template to display to the user
 */
@Controller
@RequestMapping
public class WelcomeController {

    /**
     * REQUEST url/
     * Will return index.html
     * @return - index.html
     */
    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
