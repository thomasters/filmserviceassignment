package com.thomasters.filminfoservice.entities;

import javax.persistence.*;

@Entity
@Table(name = "films")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="film_name")
    private String name;

    @Column(name="film_year")
    private String year;

    @Column(name="film_genre")
    private String genre;

    @Column(name="film_director")
    private String director;

    public Integer getId() {
        return id;
    }

    public Film setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Film setName(String name) {
        this.name = name;
        return this;
    }

    public String getYear() {
        return year;
    }

    public Film setYear(String year) {
        this.year = year;
        return this;
    }

    public String getGenre() {
        return genre;
    }

    public Film setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    public String getDirector() {
        return director;
    }

    public Film setDirector(String director) {
        this.director = director;
        return this;
    }

    public String toString(){
        return String.format("ID: %s, Name: %s, Year: %s, Genre: %s, Director: %s", id, name, year, genre, director);
    }
}
