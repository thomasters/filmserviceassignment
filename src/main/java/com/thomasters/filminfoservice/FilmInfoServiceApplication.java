package com.thomasters.filminfoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.thomasters.filminfoservice.repositories")
@EntityScan("com.thomasters.filminfoservice.entities")
@SpringBootApplication
public class FilmInfoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmInfoServiceApplication.class, args);
    }

}
